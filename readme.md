# Earthquakes in KML

 En este ejemplo se muestra como integrar una capa vectorial _KML_ y configurarla para resaltar características específicas, junto con una capa raster como mapa base. 

 Primero, se creará uan función para modificar el estilo de la capa vectorial. Está función se personalizará según las necesidades específicas de visualización. 

```js
const styleFunction = function (feature) {
}
```
Creamos por separado una capa vectorial con la dirección y nombre del archivo. Esta capa no extraerá los estilos del archivo _KML_, sino que usará los estilos definidos en `styleFunction`.

```js
const vector = new VectorLayer({
  source: new VectorSource({
    url: '2012_Earthquakes_Mag5.kml',
    format: new KML({
      extractStyles: false,
    }),
  }),
  style: styleFunction,
});
```
Y una capa raster, utilizando un servicio de mapas proporcionado por [StadiaMaps](https://stadiamaps.com/). Este mapa base utiliza la capa 'stamen_toner' para un diseño específico.

```js
const raster = new TileLayer({
  source: new StadiaMaps({
    layer: 'stamen_toner',
  }),
});
```

Para visualizar el mapa, creamos el objeto `map` especificando el contenedor HTML objetivo, las capas que incluirán tanto la raster como la vectorial, y la configuración de la vista inicial.

```js
const map = new Map({
  target: 'map',
  layers: [raster, vector],
  view: new View({
    center: [0, 0],
    zoom: 2
  })
});
```
Finalmente,  implementamos funciones para gestionar eventos de interacción como el movimiento del ratón y clics, mostrando información de la característica más cercana al cursor. Utilizamos una tooltip para mostrar los datos relevantes de cada característica seleccionada o sobre la cual se pasa el ratón.

```js
let currentFeature;
const displayFeatureInfo = function (pixel, target) {
  const feature = target.closest('.ol-control')
//
```
Y en el archivo `.css` se especificará la configuración del tooltip donde se monstrará la información que deseemos
